#include "torrent.h"
using namespace std;

Torrent::Torrent() {
	SIZE_=0;
  	SEEDER_=0;
	DOWN_=0;
}

Torrent::Torrent(const string& name, int maxSize, int seeder) {
	NAME_=name;
	SIZE_=maxSize;
	SEEDER_=seeder;
	DOWN_=0;
}

string Torrent::name() const { // 토렌트 파일의 이름
	return NAME_;
}

int Torrent::seeder() const { // seeder의 수
	return SEEDER_;
}

int Torrent::maxSize() const { // 다운로드가 완료됐을 때의 토렌트 파일의 크기
	return SIZE_;
}

int Torrent::down() const {  //다운로드 된 정도
	return DOWN_;
}

int Torrent::download(int _second) { // _second만큼 다운로드
	DOWN_=DOWN_+(SEEDER_*_second);
}
