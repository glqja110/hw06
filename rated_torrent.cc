#include "torrent.h"
#include "rated_torrent.h"
using namespace std;


RatedTorrent::RatedTorrent() {
	RATE_=0;
	SIZE_=0;
	SEEDER_=0;
	DOWN_=0;
}

RatedTorrent::RatedTorrent(const string& name, int maxSize, int seeder, int rating) {
	NAME_=name;
	SIZE_=maxSize;
	SEEDER_=seeder;
	RATE_=rating;
	DOWN_=0;
}

RatedTorrent::RatedTorrent(const string& name, int maxSize, int seeder, int rating, int down) {
	NAME_=name;
	SIZE_=maxSize;
	SEEDER_=seeder;
	RATE_=rating;
	DOWN_=down;
}

  // ‘torrent(***)' 처럼 평가 점수를 원래 이름에 추가하여 리턴 (최고점은 5점)
string RatedTorrent::stars() const {
	string stars;
	for(int i=0;i<RATE_;i++)
		stars+="*";
	return stars;
}

void RatedTorrent::PlusRate(int rating) {
	RATE_=rating;
}

string RatedTorrent::name() const { // 토렌트 파일의 이름
	return NAME_;
}

int RatedTorrent::seeder() const { // seeder의 수
	return SEEDER_;
}

int RatedTorrent::maxSize() const { // 다운로드가 완료됐을 때의 토렌트 파일의 크기
	return SIZE_;
}

int RatedTorrent::down() const {  //다운로드 된 정도
	return DOWN_;
}

int RatedTorrent::download(int _second) { // _second만큼 다운로드
	DOWN_=DOWN_+(SEEDER_*_second);
}
