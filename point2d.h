#ifndef _POINT2D_H_
#define _POINT2D_H_

#define MAX_NUM 30
#include <iostream>
#include <string>
#include <vector>


struct Point {
  int x_, y_;  // 멤버 변수.

  Point();
  explicit Point(int c);
  Point(int x, int y);
};

Point operator+(const Point& lhs, const Point& rhs);
Point operator-(const Point& lhs, const Point& rhs);
Point operator*(const Point& lhs, const Point& rhs);

#endif /* point2d.h */
