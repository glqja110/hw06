#include "point2d.h"
using namespace std;


Point::Point() {
	x_=0;
	y_=0;
}

Point::Point(int c) {
	x_=c;
	y_=c;
}


Point::Point(int x, int y) {
	x_=x;
	y_=y;
}


Point operator+(const Point& lhs, const Point& rhs) {
	Point pos(lhs.x_+rhs.x_,lhs.y_+rhs.y_);
	return pos;
}

Point operator-(const Point& lhs, const Point& rhs) {
	Point pos(lhs.x_-rhs.x_,lhs.y_-rhs.y_);
	return pos;
}

Point operator*(const Point& lhs, const Point& rhs) {
	Point pos(lhs.x_*rhs.x_,lhs.y_*rhs.y_);
	return pos;
}
