#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "torrent.h"
#include "rated_torrent.h"
using namespace std;


class TorrentClient {
 public:
  TorrentClient();
  void Add(Torrent& Torrent);
  void Rated_Add(RatedTorrent& RatedTorrent);
  void Print();
  void RatedPrint();
  void Delete(string& name_);
  void OneDelete(string& name_);
  //void FindandRate(string& _name,int rating);
  string FindNAME(string& name_);
  int FindSEED(string& name_);
  int FindSIZE(string& name_);
  int FindDOWN(string& name_);
  void Downloads(int second);
  // 필요한 함수 자유롭게 구현
 private:
  vector<Torrent> torrents_;  // 토렌트를 저장하는 벡터
  vector<RatedTorrent> ratedTorrents_;  // 평가된 토렌트를 저장하는 벡터
  // 그 외 필요한 변수를 추가
};

#endif /* torrent_client.h */
