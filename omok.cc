#include "omok.h"
using namespace std;

void Omok::setOmok() {                 //omok판을 벡터로 셋팅
	omokmap.reserve(height_);
	for(int i=0; i<height_;i++)                
		omokmap[i].resize(width_,"."); 	
	turn_=BLACK;
}

int Omok::Put(int x, int y) {
	if(x>=19||y>=19||x<0||y<0) {              //놓으려는 위치에 이미 오목알이 있을때, 혹은 놓으려는 위치가 오목판을 벗어날때
		cout<<"Can not be placed there."<<endl;
		return ERROR;
	}
	else if(omokmap[y][x]=="o"||omokmap[y][x]=="x") {
		cout<<"Can not be placed there."<<endl;
		return ERROR;
	}
	else {
		/*if(turn_=nobody)                     //첫번째 놓을 때 BLACK상태로 놓기 우선 필요할수도 있으니 작성
			turn_=BLACK;*/
		if(turn_==BLACK) {
			omokmap[y][x]="o";            //BLACK일때 o 놓기
			turn_=WHITE;                  //BLACK으로 놓았으면 turn_을 WHITE로 바꾼다
			return OK;
		}
		else if(turn_=WHITE) {
			omokmap[y][x]="x";            //WHITE일때 x 놓기
			turn_=BLACK;                  //WHITE로 놓았으면 turn_을 BLACK으로 바꾼다
			return OK;
		}
	}
}
void Omok::IsOmok() {
	for(int i=0;i<height_;i++) {                           //가로 검사
		for(int j=0;j<=width_-5;j++) {
		if(omokmap[i][j]=="o"&&omokmap[i][j+1]=="o"&&omokmap[i][j+2]=="o"&&omokmap[i][j+3]=="o"&&omokmap[i][j+4]=="o")  {    //가로로 5개가 o이면	
			if(j<14) {
				if(omokmap[i][j+5]!="o") {
					WINNER_=BLACK;
					break;
				}
			}
			else if(j==14){
				WINNER_=BLACK;
				break;
			}
		}
		
		else if(omokmap[i][j]=="x"&&omokmap[i][j+1]=="x"&&omokmap[i][j+2]=="x"&&omokmap[i][j+3]=="x"&&omokmap[i][j+4]=="x")  {    //가로로 5개가 x이면	
			if(j<14) {
				if(omokmap[i][j+5]!="x") {
					WINNER_=WHITE;
					break;
				}
			}
			else if(j==14){
				WINNER_=WHITE;
				break;
			}
		}
		
		}
	}
	for(int i=0;i<=height_-5;i++) {                                //좌측 대각선 검사
		for(int j=0;j<=width_-5;j++) {
		
		if(omokmap[i][j]=="o"&&omokmap[i+1][j+1]=="o"&&omokmap[i+2][j+2]=="o"&&omokmap[i+3][j+3]=="o"&&omokmap[i+4][j+4]=="o")  {   //좌측 대각선으로 5개가 o이면	
			if(j<14&&i<14) {
				if(omokmap[i+5][j+5]!="o") {
					WINNER_=BLACK;
					break;
				}
			}
			else if(j==14||i==14){
				WINNER_=BLACK;
				break;
			}
		}
	
		
		else if(omokmap[i][j]=="x"&&omokmap[i+1][j+1]=="x"&&omokmap[i+2][j+2]=="x"&&omokmap[i+3][j+3]=="x"&&omokmap[i+4][j+4]=="x")  {   //좌측 대각선으로 5개가 x이면	
			if(j<14&&i<14) {
				if(omokmap[i+5][j+5]!="x") {
					WINNER_=WHITE;
					break;
				}
			}
			else if(j==14||i==14){
				WINNER_=WHITE;
				break;
			}
		}
		
		}
	}
	for(int i=0;i<=height_-5;i++) {                          //세로 검사
		for(int j=0;j<width_;j++) {
		
		if(omokmap[i][j]=="o"&&omokmap[i+1][j]=="o"&&omokmap[i+2][j]=="o"&&omokmap[i+3][j]=="o"&&omokmap[i+4][j]=="o")  {    //세로로 5개가 o이면	
			if(i<14) {
				if(omokmap[i+5][j]!="o") {
					WINNER_=BLACK;
					break;
				}
			}
			else if(i==14){
				WINNER_=BLACK;
				break;
			}
		}
		
		
		else if(omokmap[i][j]=="x"&&omokmap[i+1][j]=="x"&&omokmap[i+2][j]=="x"&&omokmap[i+3][j]=="x"&&omokmap[i+4][j]=="x")  {    //세로로 5개가 x이면	
			if(i<14) {
				if(omokmap[i+5][j]!="x") {
					WINNER_=WHITE;
					break;
				}
			}
			else if(i==14){
				WINNER_=WHITE;
				break;
			}
		}
		
		}
	}
	for(int i=4;i<height_;i++) {                        //우측 대각선 검사
		for(int j=0;j<=width_-5;j++) {
	
		if(omokmap[i][j]=="o"&&omokmap[i-1][j+1]=="o"&&omokmap[i-2][j+2]=="o"&&omokmap[i-3][j+3]=="o"&&omokmap[i-4][j+4]=="o")  {   //우측 대각선으로 5개가 o이면	
			if(j<14&&i>4) {
				if(omokmap[i-5][j+5]!="o") {
					WINNER_=BLACK;
					break;
				}
			}
			else if(j==14||i==4){
				WINNER_=BLACK;
				break;
			}
		}
		
		else if(omokmap[i][j]=="x"&&omokmap[i-1][j+1]=="x"&&omokmap[i-2][j+2]=="x"&&omokmap[i-3][j+3]=="x"&&omokmap[i-4][j+4]=="x"&&omokmap[i-5][j+5]!="x")  {   //우측 대각선으로 5개가 x이면	
			if(j<14&&i>4) {
				if(omokmap[i-5][j+5]!="x") {
					WINNER_=WHITE;
					break;
				}
			}
			else if(j==14||i==4){
				WINNER_=WHITE;
				break;
			}
		}
		}
	}

	
	int check=0;                    //오목알이 꽉 채워졌는지 확인
	for(int i=0;i<height_;i++) {                           //만약 .이 하나라도 있으면 check가 1됨
		for(int j=0;j<width_;j++) {
			if(omokmap[i][j]==".")
				check=1;
		}
	}
	if(check==0)                 //check가 0(.이 하나도 없으면)이면 WINNER_는 없다
		WINNER_=NOBODY;
		
}

int Omok::WINNER() {return WINNER_;}

ostream& operator<<(ostream& os, const Omok& omok) {
	for(int i=0;i<omok.height_;i++) {
		for(int j=0;j<omok.width_;j++) {
			os<<omok.omokmap[i][j];
			if(j!=omok.width_-1)
				os<<' ';
		}
		os<<endl;
	}
	return os;
}
