#include "point2d.h"
using namespace std;

int main() {
	Point*pnt=new Point[MAX_NUM];        //구조체 배열
	vector<string> NAMES(MAX_NUM);      //이름들을 넣을 벡터
	int here=0;                         //어디에 있는지를 나타내는 변수
	while(true) {
		string inputs;
		cin>>inputs;
		if(inputs=="quit") break;
		else if(inputs=="set") {
			string NAME;              //문자 이름
			cin>>NAME;
			NAMES[here]=NAME;
			int x_,y_;
			cin>>x_>>y_;
			pnt[here].x_=x_;          //숫자 지정해 넣기
			pnt[here].y_=y_;
			here++;
		}
		else if(inputs=="eval") {
			string left;               //연산 왼쪽에 있는 애
			string oprt;               //연산자
			string right;              //연산 오른쪽에 있는 애
			cin>>left>>oprt>>right;      //left 연산자 right
			int there1=0;             //left가 어디있는지 나타내는 변수
			int there2=0;             //right가 어디있는지 나타내는 변수
			int goprt=1;                //연산을 실행할지 말지 (변수가 set해놓은게 아니였으면 goprt는 0이된다)
			
			int check1=0; 
			for(int i=0;i<left.size();i++) {                        //left가 숫자로만 되어있는지 확인
				if((int)left[i]>=48&&(int)left[i]<=57)
					check1++;
			}
			//left가 숫자로 되어있는 경우
			if(check1==left.size()) {
				NAMES[here]=left;
				int number=-1;
				for(int i=0;i<left.size();i++) {                    //문자로 표현되어 있는 숫자를 정수로 바꿔줌
					if(number<0) number=((int)(left[i]))-48;
					else number=10*number+((int)(left[i])-48);
				}       
				pnt[here].x_=number;
				pnt[here].y_=number;
				there1=here;
				here++;
			}
			else {                                   //left가 문자로 되어있던 경우
				int namech=0;
				for(int i=0;i<MAX_NUM;i++) {
					if(left==NAMES[i]) {
						namech=1;
						there1=i;
						break;
					}
				}
				if(namech==0) {                //set해놓았던 것이 아니면
					cout<<"input error"<<endl;
					goprt=0;
				}
			}					

			int check2=0; 
			for(int i=0;i<right.size();i++) {                        //right가 숫자로만 되어있는지 확인
				if((int)right[i]>=48&&(int)right[i]<=57)
					check2++;
			}
			//right가 숫자로 되어있는 경우
			if(check2==right.size()) {
				NAMES[here]=right;
				int number=-1;
				for(int i=0;i<right.size();i++) {                    //문자로 표현되어 있는 숫자를 정수로 바꿔줌
					if(number<0) number=((int)(right[i]))-48;
					else number=10*number+((int)(right[i])-48);
				}
				pnt[here].x_=number;
				pnt[here].y_=number;
				there2=here;
				here++;
			}

			else {                                   //right가 문자로 되어있던 경우
				int namech2=0;
				for(int i=0;i<MAX_NUM;i++) {
					if(right==NAMES[i]) {
						namech2=1;
						there2=i;
						break;
					}
				}
				if(namech2==0&&goprt!=0) {                //set해놓았던 것이 아니면
					cout<<"input error"<<endl;
					goprt=0;
				}
			}	
			
			if(goprt==1) {                //input error인 상황이 아니면	
				if(oprt=="+") {
					Point result=pnt[there1]+pnt[there2];
					cout<<"("<<result.x_<<", "<<result.y_<<")"<<endl;
				}
				else if(oprt=="-") {
					Point result=pnt[there1]-pnt[there2];
					cout<<"("<<result.x_<<", "<<result.y_<<")"<<endl;
				}
				else if(oprt=="*") {
					Point result=pnt[there1]*pnt[there2];
					cout<<"("<<result.x_<<", "<<result.y_<<")"<<endl;
				}
			}		
		
		}
	}		
}
