#include "torrent.h"
#include "rated_torrent.h"
#include "torrent_client.h"
using namespace std;

int main() {
	TorrentClient Client;                   //파일들 다 저장할 class
	while(true) {
		string inputs;             //처음 무언가를 입력할 변수
		cin>>inputs;	
		if(inputs=="quit") break;
		
		else if(inputs=="add") {        //inputs가 add일 때  ( add 이름 [size] [seeder] )
			string a_NAME;
			int a_SIZE, a_SEEDER;
			cin>>a_NAME>>a_SIZE>>a_SEEDER;      //이름 [size] [seeder] 순으로 입력
			//add 시키는 함수를 여기다가 작성
			Torrent a_T(a_NAME, a_SIZE, a_SEEDER);
			Client.Add(a_T);                            //입력한 것들을 Client에 추가     (그냥 Torrent)

		}
		
		else if(inputs=="download") {        //inputs가 download일 때 (download 숫자)   다운 다 되면 삭제 되는것도 고려
			int d_SPEED;         //다운 속도
			cin>>d_SPEED;
			//download랑 관련된 함수 여기다가 작성
			Client.Downloads(d_SPEED);
		
		}

		else if(inputs=="rate") {            //inputs가 rate일 때 (rate 파일이름 별갯수)
			string r_NAME;
			int r_STAR;
			cin>>r_NAME>>r_STAR;
			//rate랑 관련된 함수 여기다가 작성
			//Client.FindandRate(r_NAME,r_STAR);                 //별점 넣기
			if(r_STAR>=0&&r_STAR<=5) {               //0-5 점 제한
				RatedTorrent ra_T(r_NAME,Client.FindSIZE(r_NAME),Client.FindSEED(r_NAME),r_STAR,Client.FindDOWN(r_NAME));
				Client.Rated_Add(ra_T);
				Client.OneDelete(r_NAME);
			}

		}
		
		else if(inputs=="add_rate") {         //inputs가 add_rate일 때 ( add_rate 이름 [size] [seeder] [rating] )
			string ar_NAME;
			int ar_SIZE, ar_SEEDER, ar_STAR;
			cin>>ar_NAME>>ar_SIZE>>ar_SEEDER>>ar_STAR;
			//add_rate랑 관련된 함수 여기다가 작성
			if(ar_STAR>=0&&ar_STAR<=5) {
			RatedTorrent ar_T(ar_NAME, ar_SIZE, ar_SEEDER,ar_STAR);
			Client.Rated_Add(ar_T);                            //입력한 것들을 Client에 추가    (rated Torrent)
			}
		
		}

		else if(inputs=="print")  {           //inputs가 print일 때
			//print랑 관련된 함수 여기다가 작성
			Client.RatedPrint();
			Client.Print();
		}

		else if(inputs=="delete") {          //inputs가 delete일 때
			string d_NAME;
			cin>>d_NAME;
			//delete랑 관련된 함수 여기다가 작성
			Client.Delete(d_NAME);
		}
	}
	return 0;
}
