#ifndef _PARSE_RADIX_H_
#define _PARSE_RADIX_H_

#include <string>
#include <iostream>
using namespace std;

int ParseRadix(const string& number);
string ConvertRadix(int number, int radix);

#endif /* parse_radix.h */

