#ifndef _RATED_TORRENT_H_
#define _RATED_TORRENT_H_


#include "torrent.h"
using namespace std;



class RatedTorrent : public Torrent {
 public:
  RatedTorrent();
  RatedTorrent(const string& name, int maxSize, int seeder, int rating);
  RatedTorrent(const string& name, int maxSize, int seeder, int rating, int down);

  // ‘torrent(***)' 처럼 평가 점수를 원래 이름에 추가하여 리턴 (최고점은 5점)
  string name() const;  // 토렌트 파일의 이름
  int seeder() const;  // seeder의 수

  int maxSize() const; // 다운로드가 완료됐을 때의 토렌트 파일의 크기
  int down() const;
  int download(int _second); // _second만큼 다운로드
  string stars() const;
  void PlusRate(int rating);

  // 필요시 함수 추가 구현 가능

 private:
  int RATE_;                 //별점
  string NAME_;              //이름
  int SIZE_;                 //토렌트 최고 사이즈
  int SEEDER_;               //토렌트 시더
  int DOWN_;                 //토렌트 다운로드 된 정도
};

#endif /* torrent_client.h */
