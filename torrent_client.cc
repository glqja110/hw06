#include "torrent.h"
#include "rated_torrent.h"
#include "torrent_client.h"
using namespace std;

//토렌트 파일들이 저장되는 부분

TorrentClient::TorrentClient() {

}

void TorrentClient::Add(Torrent& Torrent) {
	torrents_.push_back(Torrent);
}

void TorrentClient::Rated_Add(RatedTorrent& RatedTorrent) {
	ratedTorrents_.push_back(RatedTorrent);
}

void TorrentClient::Print() {
	for(int i=0;i<torrents_.size();i++) {
		cout<<torrents_[i].down()<<"/"<<torrents_[i].maxSize()<<' '<<torrents_[i].name()<<endl;
	}
}

void TorrentClient::RatedPrint() {
	for(int i=0;i<ratedTorrents_.size();i++) {
		cout<<ratedTorrents_[i].down()<<"/"<<ratedTorrents_[i].maxSize()<<' '<<ratedTorrents_[i].name()<<"("<<ratedTorrents_[i].stars()<<")"<<endl;
	}	
}

void TorrentClient::Downloads(int second) {
	int check1=1;
	int check2=1;
	for(int i=0;i<torrents_.size();i++) 
		torrents_[i].download(second);
	for(int i=0;i<ratedTorrents_.size();i++) 
		ratedTorrents_[i].download(second);
	while(check1==1) {
	check1=0;
	vector<Torrent>::iterator it_;
	for(it_=torrents_.begin();it_!=torrents_.end();it_++) {
		if(it_->down()>=it_->maxSize()) {
			check1=1;
			break;
		}
	}
	if(check1==1) torrents_.erase(it_);
	}
	while(check2==1) {
	check2=0;
	vector<RatedTorrent>::iterator its;
	for(its=ratedTorrents_.begin();its!=ratedTorrents_.end();its++) {
		if(its->down()>=its->maxSize()) {
			check2=1;
			break;
		}
	}
	if(check2==1) ratedTorrents_.erase(its);
	}
}

string TorrentClient::FindNAME(string& name_) {
	int number;
	for(int i=0;i<torrents_.size();i++) {
		if(torrents_[i].name()==name_) {
			number=i;
			break;
		}
	} 
	return torrents_[number].name();
}
int TorrentClient::FindSEED(string& name_) {
	int number;
	for(int i=0;i<torrents_.size();i++) {
		if(torrents_[i].name()==name_) {
			number=i;
			break;
		}
	} 
	return torrents_[number].seeder();
}

int TorrentClient::FindSIZE(string& name_) {
	int number;
	for(int i=0;i<torrents_.size();i++) {
		if(torrents_[i].name()==name_) {
			number=i;
			break;
		}
	} 
	return torrents_[number].maxSize();
}

int TorrentClient::FindDOWN(string& name_) {
	int number;
	for(int i=0;i<torrents_.size();i++) {
		if(torrents_[i].name()==name_) {
			number=i;
			break;
		}
	} 
	return torrents_[number].down();
}

void TorrentClient::OneDelete(string& name_) {
	vector<Torrent>::iterator it_;
	if(torrents_.size()>0) {
	for(it_=torrents_.begin();it_!=torrents_.end();it_++) {
		if(it_->name()==name_)
			break;
	}
	torrents_.erase(it_);
	}
}


void TorrentClient::Delete(string& name_) {
	int check1=0;
	int check2=0;
	vector<Torrent>::iterator it_;
	if(torrents_.size()>0) {
	for(it_=torrents_.begin();it_!=torrents_.end();it_++) {
		if(it_->name()==name_) {
			check1=1;
			break;
		}
	}
	if(check1==1) torrents_.erase(it_);
	}
	vector<RatedTorrent>::iterator its;
	if(ratedTorrents_.size()>0) {
	for(its=ratedTorrents_.begin();its!=ratedTorrents_.end();its++) {
		if(its->name()==name_) {
			check2=1;
			break;
		}
	}
	if(check2==1) ratedTorrents_.erase(its);
	}
	if(check1==0&&check2==0)
		cout<<"invalid operation."<<endl;
}
