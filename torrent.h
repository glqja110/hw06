#ifndef _TORRENT_H_
#define _TORRENT_H_

#include <iostream>
#include <vector>
#define MAX_NUM 30

using namespace std;


class Torrent {
 public:
  Torrent();
  Torrent(const string& name, int maxSize, int seeder);

  string name() const;  // 토렌트 파일의 이름
  int seeder() const;  // seeder의 수

  int maxSize() const; // 다운로드가 완료됐을 때의 토렌트 파일의 크기
  int down() const;
  int download(int _second); // _second만큼 다운로드
  // 다운로드에 대한 자세한 설명은 아래 참조

  // 필요시 함수 추가 구현 가능

 private:
  string NAME_;              //이름
  int SIZE_;                 //토렌트 최고 사이즈
  int SEEDER_;               //토렌트 시더
  int DOWN_;                 //토렌트 다운로드 된 정도
  // 멤버변수를 정의.
};

#endif /* torrent.h */
