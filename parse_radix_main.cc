#include "parse_radix.h"
using namespace std;

int main() {
	while(true) {
		string inputs;        //eval 아니면 quit 입력할 스트링.
		cin>>inputs;
		if(inputs=="quit") break;        //quit 입력하면 종료
		else if(inputs=="eval") {
			string num_radix1;            //[숫자]_[진법] 첫번째.		
			cin>>num_radix1;
			string oprt;        //연산자 입력
			cin>>oprt;
			if(oprt=="+") {
				string num_radix2;
				cin>>num_radix2;
				int radix_;              //그 다음에 몇진법으로 표현할건지 작성함.
				cin>>radix_;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					int total=num1+num2;
					cout<<ConvertRadix(total,radix_)<<endl;
				}
				else cout<<"Error"<<endl;
			}
			else if(oprt=="-") {
				string num_radix2;
				cin>>num_radix2;
				int radix_;              //그 다음에 몇진법으로 표현할건지 작성함.
				cin>>radix_;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					int total=num1-num2;
					cout<<ConvertRadix(total,radix_)<<endl;
				}
				else cout<<"Error"<<endl;
			}
			else if(oprt=="*") {
				string num_radix2;
				cin>>num_radix2;
				int radix_;              //그 다음에 몇진법으로 표현할건지 작성함.
				cin>>radix_;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					int total=num1*num2;
					cout<<ConvertRadix(total,radix_)<<endl;
				}
				else cout<<"Error"<<endl;
			}
			else if(oprt=="/") {
				string num_radix2;
				cin>>num_radix2;
				int radix_;              //그 다음에 몇진법으로 표현할건지 작성함.
				cin>>radix_;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					int total=num1/num2;
					cout<<ConvertRadix(total,radix_)<<endl;
				}
				else cout<<"Error"<<endl;
			}
			else if(oprt=="<") {
				string num_radix2;
				cin>>num_radix2;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					if(num1<num2) cout<<"true"<<endl;
					else cout<<"false"<<endl;
				}
				else cout<<"Error"<<endl;
			}
			else if(oprt==">") {
				string num_radix2;
				cin>>num_radix2;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					if(num1>num2) cout<<"true"<<endl;
					else cout<<"false"<<endl;
				}
				else cout<<"Error"<<endl;
			}
			else if(oprt=="==") {
				string num_radix2;
				cin>>num_radix2;
				int num1,num2;	
				num1=ParseRadix(num_radix1);
				num2=ParseRadix(num_radix2);
				if(num1!=-1&&num2!=-1) {
					if(num1==num2) cout<<"true"<<endl;
					else cout<<"false"<<endl;
				}
				else cout<<"Error"<<endl;
			}

		}
	}
	return 0;
}
