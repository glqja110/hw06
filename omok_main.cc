#include "omok.h"

int main() {
	Omok omk;
	omk.setOmok();
	while(true) {
		if(omk.Turn()==BLACK)
			cout<<"Black: ";
		else if(omk.Turn()==WHITE)
			cout<<"White: ";
		int x1_,y1_;
		cin>>x1_>>y1_;                  //어디에 놓을지 입력
		
		if(omk.Put(x1_,y1_)!=-1) {               //잘못 두지 않았을 때
			omk.IsOmok();             //승자 있는지 확인
			if(omk.WINNER()==BLACK) {
				cout<<omk;
				cout<<"Winner: Black Player"<<endl;
				break;
			}
			else if(omk.WINNER()==WHITE) {
				cout<<omk;
				cout<<"Winner: White Player"<<endl;
				break;
			}
			else if(omk.WINNER()==NOBODY) {
				cout<<omk;
				break;
			}
		}
	}
	return 0;
}
	
			
