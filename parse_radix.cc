#include "parse_radix.h"
using namespace std;

int ParseRadix(const string& number) {
	string numberP="";       //숫자 부분
	int radixP=-1;       //진법 숫자 부분
	int numberPP=0;         //numberP를 진법으로 표현해서 나오는 숫자. 
	string letter="";        //임시로 저장할 string.
	int check=0;          //'_'가 있는지 확인하는 변수.
	int error=0;          //숫자가 진법숫자보다 크거나 같은 경우일 때 1 로 되서 error를 나타냄.
	for(int i=0; i<number.size(); ++i) {
            char c = number[i];
	    if(c!='_') letter+=c;
            else if(c == '_') {
		numberP=letter;
		letter="";
		check=1;
	    }
        }
	if(check==0) numberP=letter;
	else if(check==1) {
		for(int i=0;i<letter.size();i++) {
			if(radixP<0) radixP=(int)(letter[i]-48);
			else radixP=radixP*10+(int)(letter[i]-48);
		}
	}
	//string으로 되어있는 numberP와 int로 설정한 radixP를 다 정해놓음.

	if(check==1) {                                       //_가 있는 경우
		int count=0;                   //몇번 제곱해서 더해나갈것인지
		for(int i=numberP.size()-1;i>=0;i--) {
			int num=(int)numberP[i];
			if(num>=48&&num<=57) {                    //0~9 숫자일 경우
				int digit=num-48;
				if(digit>=radixP) error=1;           //숫자가 error인 경우.
				else {
					int square=1;
					for(int j=0;j<count;j++) {
						square=square*radixP;
					}
					numberPP+=digit*square;
					count++;
				}
			}
			else if(num>=97&&num<=122) {              //a~z 인경우
				int alpha=num-87;
				if(alpha>=radixP) error=1;          //숫자가 error인 경우.
				else {
					int square=1;
					for(int j=0;j<count;j++) {
						square=square*radixP;
					}
					numberPP+=alpha*square;
					count++;
				}
			}
			/*else if(num>=65&&num<=90) {              //A~Z 인경우 (우선 주석처리 다는데, 대문자를 소문자로 인식해야 하는 경우 이 주석 풀기) 
				int alpha2=num-55;
				if(alpha>=radixP) error=1;          //숫자가 error인 경우.
				else {
					int square=1;
					for(int j=0;j<count;j++) {
						square=square*radixP;
					}
					numberPP+=alpha*square;
					count++;
				}
			}*/
		}
		if(error==0) {			
			return numberPP;          //진법으로 고쳐서 나온 숫자인 numberPP를 반환 에러가 없을시
		}
		else return -1;
	}

	else {                  //_가 없는 경우   (10진법일때)
		int count=0;                   //몇번 제곱해서 더해나갈것인지
		for(int i=numberP.size()-1;i>=0;i--) {
			int num=(int)numberP[i];
			if(num>=48&&num<=57) {                    //0~9 숫자일 경우
				int digit=num-48;
				if(digit>=10) error=1;           //숫자가 error인 경우.
				else {
					int square=1;
					for(int j=0;j<count;j++) {
						square=square*10;
					}
					numberPP+=digit*square;
					count++;
				}
			}
			else if(num>=97&&num<=122) {              //a~z 인경우
				int alpha=num-87;
				if(alpha>=10) error=1;          //숫자가 error인 경우.
				else {
					int square=1;
					for(int j=0;j<count;j++) {
						square=square*10;
					}
					numberPP+=alpha*square;
					count++;
				}
			}
			/*else if(num>=65&&num<=90) {              //A~Z 인경우 (우선 주석처리 다는데, 대문자를 소문자로 인식해야 하는 경우 이 주석 풀기) 
				int alpha2=num-55;
				if(alpha>=10) error=1;          //숫자가 error인 경우.
				else {
					int square=1;
					for(int j=0;j<count;j++) {
						square=square*10;
					}
					numberPP+=alpha*square;
					count++;
				}
			}*/
		}
		if(error==0) return numberPP;          //진법으로 고쳐서 나온 숫자인 numberPP를 반환 에러가 없을시
		else return -1;
	}

}	

string ConvertRadix(int number, int radix) {
	int checkm=0;                 //number가 음수인지 체크
	string snumber="";                //진법으로 고쳐서 나오는 문자
	if(number<0) {
		checkm=1;
		number*=-1;            //음수를 양수로 고침
	}
	if(radix<10) {                 //radix가 10보다 작을 때
		while(number>0) {
			int R=number%radix;         //숫자를 해당 진수로 나눈 나머지
			snumber=((char)(R+48))+snumber;           //나머지를 더함
			number=number/radix;
		}
	}

	else if(radix==10) {
		while(number>0) {
			int R=number%radix;         //숫자를 해당 진수로 나눈 나머지
			snumber=((char)(R+48))+snumber;           //나머지를 더함
			number=number/radix;
		}
	}
	
	else {
		while(number>0) {
			int R=number%radix;         //숫자를 해당 진수로 나눈 나머지
			if(R>=10)                     //나머지가 10보다 크면 알파벳으로 나타냄
				snumber=((char)(R+87))+snumber;
			else                          //아니면 걍 숫자로 나타냄
				snumber=((char)(R+48))+snumber;          
     
			number=number/radix;
		}
	}
	string sradix="";
	if(radix<10)
		sradix+=(char)(radix+48);
	else {
		sradix=sradix+((char)(radix/10+48));
		sradix=sradix+((char)(radix%10+48));
	}
	if(radix!=10)  {        //radix가 10이 아니면
		snumber=snumber+"_"+sradix;
		if(checkm==1) snumber="-"+snumber;
		return snumber;
	}
	else {
		if(checkm==1) snumber="-"+snumber;
		return snumber;
	}

}
